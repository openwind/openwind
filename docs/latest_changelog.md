### Added
- new informations in the anaconda package (source link and description)
- modules, macros and documentation to create 3D objects from Openwind geoemtry files using FreeCAD. (still under test)

### Changed
- possibility to read impedance file with french convention for the decimal separator using the keyword `decimal_sep`
- update openwind to be compatible with nump2