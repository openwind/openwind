
First step
==========

You've successfully :doc:`installed <installation>` Openwind, now let's get it started !

Basic Tutorial
--------------

Follow the :doc:`Basic Tutorial <basic_tutorial>` to get acquainted with Openwind and run simulations.

How to
------

Follow the :doc:`HOW TO <examples>` to be guided in the different features of Openwind.

Examples
--------

*Note:* If you installed ``openwind`` with ``pip`` or ``conda`` , you can download the ``examples/`` `following this link <https://gitlab.inria.fr/openwind/openwind/-/archive/master/openwind-master.zip?path=examples>`_

In folder ``examples/`` you will find the scripts corresponding to the ``How To`` examples. They use the toolbox to:

* compute and display the impedance of a trumpet, and of an oboe with thoneholes.
* compute and display the sound of instruments with different embouchures
* perform inversions and reconstructions of instruments with side holes
* and more...

Each folder contains examples related to the one module of Openwind (technical, frequential, temporal, inversion).

Other folders contains the scripts used in scientific publications:

* ``Tournemenne-Chabassier_ACTA2019`` for `this paper <https://hal.archives-ouvertes.fr/hal-01963674>`_.
* ``Ernoult-Chabassier-Rodriguez_Humeau_2021`` for `this article <https://hal.inria.fr/hal-03231946>`_
* ``Thibault-Chabassier_JASA2020`` for `this publication <https://hal.science/hal-03328715>`_.
* ``Thibault-Chabassier-Boutin-Helie_JSV2022`` for `this scientific article <https://hal.science/hal-03794474>`_
* ``RR-humidity_Ernoult_2023`` for `this research report <https://inria.hal.science/hal-04008847v2>`_
* ``Thibault_Thesis2023`` for `the Ph.D. thesis of A.Thibault <https://theses.fr/2023PAUU3051>`_.

All the related scientific articles are visible `here <https://openwind.inria.fr/contributions/>`_.

Code Structure
--------------

To understand how the code is structured, you can go to the :doc:`Code Structure section <structure>`
