:py:mod:`ow_to_freecad<openwind.technical.ow_to_freecad>` module
=====================================================================================

.. automodule:: openwind.technical.ow_to_freecad
   :members:
   :undoc-members:
   :show-inheritance:
