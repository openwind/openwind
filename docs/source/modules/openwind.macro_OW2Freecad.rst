:py:mod:`macro_OW2Freecad<openwind.macro_OW2Freecad>` module
===============================================================================================

.. automodule:: openwind.macro_OW2Freecad
   :members:
   :undoc-members:
   :show-inheritance:
