before_script:
  # - source ~/virtEnvOpenwind/bin/activate
  - python --version
  - whoami
  - export PYTHONPATH="${PYTHONPATH}:$(pwd)" # add current dir to PYTHONPATH, for test purpose
  - echo $PYTHONPATH

stages:
  - test
  - release
  - update_pip
  - update_conda
  - build_doc

Release Update:
  tags:
    - release
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_PROJECT_PATH != "openwind/openwind"
      when: never                                # Do not run this outside of official repository
    - if: $CI_COMMIT_TAG                         # Run this job when a tag is created manually
  script:
    - echo 'running release_job'
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: 'docs/latest_changelog.md'
    tag_name: '$CI_COMMIT_TAG' # elsewhere in the pipeline.
    ref: '$CI_COMMIT_TAG'

Pip Update:
  tags:
    - pip
  stage: update_pip
  rules:
    - if: $CI_PROJECT_PATH != "openwind/openwind"
      when: never                                # Do not run this outside of official repository
    - if: $CI_COMMIT_TAG                         # Run this job when a tag is created manually

  script:
    - python setup.py sdist bdist_wheel
    - TWINE_PASSWORD=${PYPI_API_TOKEN} TWINE_USERNAME=__token__ python -m twine upload dist/*

Conda Update:
  tags:
    - conda
  stage: update_conda
  rules:
    - if: $CI_PROJECT_PATH != "openwind/openwind"
      when: never                                # Do not run this outside of official repository
    - if: $CI_COMMIT_TAG                         # Run this job when a tag is created manually

  script:
    # - deactivate # switch to conda environment to upload
    - conda activate # switch to conda environment to upload
    - conda config --set anaconda_upload no
    - CONDA_BLD_PATH=~/anaconda3/conda-bld
    - conda build . --no-include-recipe # build conda package but only include library
    - anaconda -t ${CONDA_UPLOAD_TOKEN} upload -u openwind ${CONDA_BLD_PATH}/noarch/openwind-*.tar.bz2
    - conda build purge-all
    - conda deactivate

Design:
  tags:
    - test
  script:
    - cd tests
    - python design/test_design.py

Technical:
  tags:
    - test
  script:
    - cd tests
    - python technical/test_instrument_geometry.py
    - python technical/test_adjust_instrument_geometry.py
    - python technical/test_player_and_instrument_physics.py
    - python technical/test_unit_options.py

Continuous:
  tags:
    - test
  script:
    - cd tests
    - python continuous/unit_test_netlist.py
    - python continuous/test_instrument_physics.py
    - python continuous/test_radiation.py
    - python continuous/test_humidity_carbon.py

Discretization:
  tags:
    - test
  script:
    - cd tests
    - python discretization/test_meshing.py
    - python discretization/test_mesh_from_lists.py

Frequential:
  tags:
    - test
  script:
    - cd tests
    - python frequential/test_interp.py
    - python frequential/test_TMMFEM.py
    - python frequential/test_TMM_spherical.py
    - python frequential/functionality_test_lossless_cylinder.py
    - python frequential/functionality_test_lossy_cylinder.py
    - python frequential/functionality_test_cone.py
    - python frequential/functionality_test_cone_grad_temp.py
    - python frequential/test_GradT_adim.py
    - python frequential/section_discontinuity.py
    - python frequential/test_freq_radiation.py
    - python frequential/test_freq_valve.py
    - python frequential/test_losses_besselnew_sh.py
    - python frequential/test_modal.py
    - python frequential/test_cone_humidity_carbon.py
    - python frequential/test_admittance_flute.py

Frequential_diffrepr:
  tags:
    - test
  script:
    - cd tests
    - python frequential/diffrepr_vs_bessel.py
    - python frequential/diffrepr_vs_diffrepr-plus.py
    - python frequential/unit_adim_for_all_thermoviscous_models.py

Frequential_fingering:
  tags:
    - test
  script:
    - cd tests
    - python frequential/test_freq_fingering.py

Temporal:
  tags:
    - test
  script:
    - cd tests
    - python temporal/test_energy_conservative_components.py
    - python temporal/test_energy_dissipative_components.py
    - python temporal/test_treed_contactNL.py
    - python temporal/test_executescore.py
    - python temporal/test_compare_audio.py
#    - python test/test_score.py     # Add again when temporal simu is more stable

Inversion:
  tags:
    - test
  script:
    - cd tests
    - python inversion/homemade_algo_optim.py
    - python inversion/diff_observable.py
    - python inversion/gradient_computation.py
    - python inversion/sensitivity_observable.py
    - python inversion/inversion_section_discontinuity.py
    - python inversion/inversion_1param_unbounded.py
    - python inversion/inversion_3params_bounded.py
    - python inversion/Linear_and_NonLinear_constraints.py

Freq_vs_Temp:
  tags:
    - test
  script:
    - cd tests
    - python test_fem_temporal.py
